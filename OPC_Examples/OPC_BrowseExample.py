# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 06:46:02 2022

@author: neidherr
"""

import asyncio
from asyncua import Client
import nest_asyncio
nest_asyncio.apply()


async def main():
    client = Client(url='opc.tcp://DLPC104:48010/', timeout=4)
    await client.connect()
    obj = client.get_objects_node()
    refs = await obj.get_references()
    print(str(obj))
    print('--------------------')
    print(str(refs))
    print('--------------------')
    print('--------------------')
    for each in refs:
        print(str(client.get_node(each.NodeId)))
        print('--------------------')
    print('--------------------')
    nextobj = client.get_node("ns=2;s=Demo")
    print(str(nextobj))
    nextrefs = await nextobj.get_references()
    print('--------------------')
    print(str(nextrefs))
    await client.disconnect()

if __name__ == "__main__":
    asyncio.run(main())
