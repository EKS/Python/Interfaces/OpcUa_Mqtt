# -*- coding: utf-8 -*-
"""
Created on Wed Jul 27 07:49:45 2022

@author: neidherr
"""

import asyncio
from asyncua import Client, ua
import nest_asyncio
nest_asyncio.apply()


async def main():
    client = Client(url='opc.tcp://DLPC104:48010/', timeout=4)
    await client.connect()
    obj = client.get_node("ns=2;s=Demo.Static.Scalar.Double")
    print(str(obj))
    print('--------------------')
    value = await obj.get_value()
    print(str(value))
    print('--------------------')
    await obj.set_value(ua.DataValue(ua.Variant(300.0, ua.VariantType.Double)))
    await client.disconnect()

if __name__ == "__main__":
    asyncio.run(main())
