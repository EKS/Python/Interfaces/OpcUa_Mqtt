# -*- coding: utf-8 -*-
"""
This is an interface between OPC UA and MQTT. It subscribes to a list of
MQTT topics and publish them to OPC nodes.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
D. Neidherr, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: D.Neidherr@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL.
"""

import logging
import platform
# import os
import re
import sys
import datetime
import paho.mqtt.client as mqtt

import asyncio
from asyncua import Client, ua
import nest_asyncio
nest_asyncio.apply()

q = asyncio.Queue()

logging.basicConfig(
    filename="mqtt2opc.log",
    level=logging.WARNING,
    style="{",
    format="{asctime} [{levelname:8}] {message}",
    datefmt="%d.%m.%Y %H:%M:%S"
)

hostname = platform.node()
client_name = hostname + "_" + "opc2mqtt"  # re.split(".py", os.path.basename(__file__))[0]
# client_name = hostname + "_" + re.split(".py", os.path.basename(__file__))[0]
date_time = re.split(" ", str(datetime.datetime.now()))
client_id = client_name  # + "_" + date_time[0] + "_" + date_time[1]


def on_mqtt_connect(client, userdata, flags, rc):
    """
    Handle MQTT broker connected callback.

    Publish all topic once.
    Subscribe to desired topics.
    """
    if rc == 0:
        client.connected_flag = True
        logging.info("Connection OK")
    else:
        logging.error("Bad connection, returned code =", rc)


def on_mqtt_disconnect(client, userdata, flags, rc=0):
    """Handle MQTT broker disconnected callback."""
    # client.connected_flag = False
    logging.info("Disconnected result code =" + str(rc))


def on_mqtt_subscribe(client, userdata, mid, granted_qos):
    """Handle subscribed callback."""
    logging.info("Client subscribed message ID =", mid, "with qos =", granted_qos)


def on_mqtt_message(client, userdata, msg):
    """
    Handle MQTT message received callback.

    Decode received MQTT message data and insert into command processor queue.
    """
    m_decode = str(msg.payload.decode("utf-8", "ignore"))
    logging.info("Message received. Topic:", msg.topic, "Payload:", m_decode)
    q.put_nowait((msg.topic, m_decode))


class Mqtt2OPC():
    def __init__(self):
        self._connected = False
        self._opcnodes = []
        self._mqtt_topics = []
        self._opcsubscribed = False
        self._background_tasks = set()

    async def _mqtt_command_processor(self):
        """
        Implement command processing here.

        Get decoded message from queue and process.
        Example: Handle topic 'Command'.
        """
        print("cmd Processor running")
        while True:
            received = await q.get()
            topic = received[0]
            data = received[1]
            logging.info("Command processing:", topic)
            if topic in self._mqtt_topics:
                datatype = await self._opcclient.get_node(self._opcnodes[self._mqtt_topics.index(topic)]).read_data_type_as_variant_type()
                print(datatype)
                print(type(datatype))
                if datatype is ua.VariantType.String:
                    await self._opcnodes[self._mqtt_topics.index(topic)].set_value(ua.DataValue(ua.Variant(data, ua.VariantType.String)))
                elif datatype is ua.VariantType.Float:
                    await self._opcnodes[self._mqtt_topics.index(topic)].set_value(ua.DataValue(ua.Variant(float(data), ua.VariantType.Float)))
                elif datatype is ua.VariantType.Double:
                    await self._opcnodes[self._mqtt_topics.index(topic)].set_value(ua.DataValue(ua.Variant(float(data), ua.VariantType.Double)))
                else:
                    print(type(datatype))
            else:
                logging.error("Received unkown:", topic)
            logging.info("Command processing done.")
            q.task_done()

    async def open_connection(self, OPCUrl, MQTTBroker="localhost"):
        """
        Opens the connection to the OPC server as well as the MQTT broker.

        Parameters
        ----------
        OPCUrl : String
            URL to the OPC server. The format is 'opc.tcp://xxx:48010/', where 48010 is the default opc port.
        MQTTBroker : String
            URL to the MQTT broker.

        Returns
        -------
        None.

        """
        if not self._connected:
            self._mqttbroker = MQTTBroker
            self._opcclient = Client(url=OPCUrl, timeout=4)
            await self._opcclient.connect()
            self._mqttclient = mqtt.Client(client_id, clean_session=True)
            self._mqttclient.connected_flag = False
            self._mqttclient.will_set(client_id, "Offline", 1, False)
            self._mqttclient.on_connect = on_mqtt_connect
            self._mqttclient.on_disconnect = on_mqtt_disconnect
            self._mqttclient.on_subscribe = on_mqtt_subscribe
            self._mqttclient.on_message = on_mqtt_message
            self._mqttclient.loop_start()
            self._mqttclient.connect(self._mqttbroker, port=1883, keepalive=60, bind_address="")
            while not self._mqttclient.connected_flag:
                logging.info("Waiting for", self._mqttbroker, "...")
                await asyncio.sleep(1)
            self._subscriptiontask = asyncio.create_task(self._mqtt_command_processor())
            self._background_tasks.add(self._subscriptiontask)
            self._connected = True

    async def subscribe(self, NodeIDs):
        """
        Subscribes to MQTT topics and updates OPC. This function can be executed
        mulitple times and just adds NodeIDs.

        Parameters
        ----------
        Nodes : OPCNodeID
            List with OPCNodeIDs to subscribe. The NodeIDs are strings of the form:
            'ns=Y;s=zzz', where Y is the OPCNameSpace number and zzz the OPCIdentifier.

        Returns
        -------
        None.

        """
        for eachNodeID in NodeIDs:
            opcnode = self._opcclient.get_node(eachNodeID)
            opcpathlist = await opcnode.get_path(as_string=(True))
            opcpath = client_id
            del opcpathlist[0]
            for each in opcpathlist:
                opcpath += "/" + re.split(":", each)[1]
            rc, mid = self._mqttclient.subscribe(opcpath)
            logging.info("Subscribing to: '", eachNodeID, "'returned rc =", rc, "mid = ", mid)
            self._opcnodes.append(self._opcclient.get_node(eachNodeID))
            self._mqtt_topics.append(opcpath)

    async def unsubscribe(self, NodeIDs=[]):
        """
        Cancel existing MQTT subscriptions.If the NodeIDs list is kept empty
        this function cancels all subscriptions.

        Parameters
        ----------
        Nodes : OPCNodeID
            List with OPCNodeIDs to unsubscribe. The NodeIDs are strings of the form:
            'ns=Y;s=zzz', where Y is the OPCNameSpace number and zzz the OPCIdentifier.
            The default is [].

        Returns
        -------
        None.

        """
        if NodeIDs == []:
            for eachMqttTopic in self._mqtt_topics:
                rc, mid = self._mqttclient.unsubscribe(eachMqttTopic)
                logging.info("Unsubscribing to: '", eachMqttTopic, "'returned rc =", rc, "mid = ", mid)
            self._opcnodes = []
            self._mqtt_topics = []
        else:
            for eachNodeID in NodeIDs:
                opcnode = self._opcclient.get_node(eachNodeID)
                opcpathlist = await opcnode.get_path(as_string=(True))
                opcpath = client_id
                del opcpathlist[0]
                for each in opcpathlist:
                    opcpath += "/" + re.split(":", each)[1]
                rc, mid = self._mqttclient.unsubscribe(opcpath)
                logging.info("Unsubscribing to: '", eachNodeID, "'returned rc =", rc, "mid = ", mid)
                self._opcnodes.remove(self._opcclient.get_node(eachNodeID))
                self._mqtt_topics.remove(opcpath)

    async def close_connection(self):
        """
        Closes the connection to the OPC server and the MQTT broker.

        Returns
        -------
        None.

        """
        self._subscriptiontask.cancel()
        self._subscriptiontask.add_done_callback(self._background_tasks.discard)
        await self._opcclient.disconnect()
        logging.info("Publishing: 'disconnected'")
        rc, mid = self._mqttclient.publish(client_id, "disconnected")
        logging.info("Publishing: 'disconnected' returned rc =", rc, "mid = ", mid)
        logging.info("Disonnecting from broker", self._mqttbroker)
        self._mqttclient.disconnect()
        await asyncio.sleep(1)
        logging.info("Stopping message loop")
        self._mqttclient.loop_stop(force=False)
        self._connected = False


async def mqtt2opc():
    try:
        Mqtt2OPCIntstance = Mqtt2OPC()
        await Mqtt2OPCIntstance.open_connection('opc.tcp://DLPC104:48010/', "localhost")
        await Mqtt2OPCIntstance.subscribe(["ns=2;s=Demo.Static.Scalar.String", "ns=2;s=Demo.Static.Scalar.Int32", "ns=2;s=Demo.Static.Scalar.Float", "ns=2;s=Demo.Static.Scalar.Double"])
        # await Mqtt2OPCIntstance.unsubscribe(["ns=2;s=Demo.Static.Scalar.String"])
        # await Mqtt2OPCIntstance.unsubscribe()
        # await Mqtt2OPCIntstance.subscribe(["ns=2;s=Demo.Static.Scalar.String"])
        while Mqtt2OPCIntstance._mqttclient.connected_flag:
            await asyncio.sleep(1)
    except BaseException as e:
        logging.error("Exception catched!", e)
        Mqtt2OPCIntstance._mqttclient.connected_flag = False
    finally:
        await Mqtt2OPCIntstance.unsubscribe()
        await Mqtt2OPCIntstance.close_connection()
        logging.info("End")
        logging.shutdown()
        sys.exit()


async def main():
    await mqtt2opc()


if __name__ == "__main__":
    asyncio.run(main())
