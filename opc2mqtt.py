#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is an interface between OPC UA and MQTT. It subscribes to a list of
OPC nodes and publish them to MQTT topics.

Copyright 2022 GSI Helmholtzzentrum für Schwerionenforschung GmbH
D. Neidherr, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: D.Neidherr@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

Published under EUPL.
"""

import paho.mqtt.client as mqtt
import platform
import os
import re
import datetime
import sys
import json
import logging

import asyncio
from asyncua import Client, ua, Node
import nest_asyncio
nest_asyncio.apply()

logging.basicConfig(
    filename="opc2mqtt.log",
    level=logging.WARNING,
    style="{",
    format="{asctime} [{levelname:8}] {message}",
    datefmt="%d.%m.%Y %H:%M:%S"
)

hostname = platform.node()
client_name = hostname + "_" + re.split(".py", os.path.basename(__file__))[0]
date_time = re.split(" ", str(datetime.datetime.now()))
client_id = client_name  # + "_" + date_time[0] + "_" + date_time[1]


def on_connect(client, userdata, flags, rc):
    """
    Handle MQTT broker connected callback.

    Publish all topic once.
    Subscribe to desired topics.
    """
    if rc == 0:
        client.connected_flag = True
        logging.info("Connection OK")
    else:
        logging.error("Bad connection, returned code =", rc)
    pass


def on_disconnect(client, userdata, flags, rc=0):
    """Handle MQTT broker disconnected callback."""
    # client.connected_flag = False
    logging.info("Disconnected result code =" + str(rc))
    pass


def on_publish(client, userdata, mid):
    """Handle MQTT publish callback."""
    # print("Client published message ID =", mid)
    pass


class SubscriptionHandler():
    async def datachange_notification(self, node: Node, val, data):
        """
        Callback for asyncua Subscription.
        This method will be called when the Client received a data change message from the Server.
        """
        # DO NOT DO BLOCKING TASKS HERE , IF YOU NEED TO PROCESS THE DATA PUT IT ON A QUEUE AND PUT IT IN A SUBPROCESS (E.G. SQL QUERYS)!
        logging.debug(f"Datachange: {data.monitored_item.Value.Value.Value}")
        self.mqttclient.publish(self.opcpath[str(node)], json.dumps(val))


class OPC2Mqtt():
    def __init__(self):
        self._handler = SubscriptionHandler()  # get an instance of the SubscriptionHandler-Class
        self._subscribedNodes = set()
        self._connected = False
        self._opcsubscribed = False

    async def open_connection(self, OPCUrl, MQTTBroker="localhost"):
        """
        Opens the connection to the OPC server as well as the MQTT broker.

        Parameters
        ----------
        OPCUrl : String
            URL to the OPC server. The format is 'opc.tcp://xxx:48010/', where 48010 is the default opc port.
        MQTTBroker : String
            URL to the MQTT broker.

        Returns
        -------
        None.

        """
        if not self._connected:
            self._mqttbroker = MQTTBroker
            self._opcclient = Client(url=OPCUrl, timeout=4)
            await self._opcclient.connect()
            self._mqttclient = mqtt.Client(client_id, clean_session=True)
            self._mqttclient.connected_flag = False
            self._mqttclient.will_set(client_id, "Offline", 1, False)
            self._mqttclient.on_connect = on_connect
            self._mqttclient.on_disconnect = on_disconnect
            self._mqttclient.on_publish = on_publish
            self._mqttclient.loop_start()
            self._mqttclient.connect(self._mqttbroker, port=1883, keepalive=60, bind_address="")
            while not self._mqttclient.connected_flag:
                logging.info("Waiting for", self._mqttbroker, "...")
                await asyncio.sleep(1)
            self._connected = True

    async def subscribe(self, Nodes):
        """
        Subscribe to OPC variables and prepare the MQTT broker. Note that this
        function can be executed mulitple times. It will then disconnect the previous
        Nodes list and reconnect with the new list. If Nodes should be added or
        deleted, subscribe_add and subscribe_sub should be used.

        Parameters
        ----------
        Nodes : OPCNodeID
            List with OPCNodeIDs to subscribe. The NodeIDs are strings of the form:
            'ns=Y;s=zzz', where Y is the OPCNameSpace number and zzz the OPCIdentifier.

        Returns
        -------
        None.

        """
        if self._opcsubscribed:
            await self.unsubscribe()
        self._opcnodes = []
        self._handleropcpath = {}
        self._opcsubscribed = True
        for eachNode in Nodes:
            opcnode = self._opcclient.get_node(eachNode)
            opcpathlist = await opcnode.get_path(as_string=(True))
            opcpath = client_id
            del opcpathlist[0]
            for each in opcpathlist:
                opcpath += "/" + re.split(":", each)[1]
            self._opcnodes.append(opcnode)
            self._handleropcpath.update({eachNode: opcpath})
        self._handler.opcpath = self._handleropcpath
        self._handler.mqttclient = self._mqttclient
        self._subscription = await self._opcclient.create_subscription(
            period=1000,  # the client will send each 1000 ms a publishrequest and the server responds with the changes since last publishrequest
            handler=self._handler,  # SubscriptionHandler which will be used for processing the notifications in the publishresponse
            publishing=True
        )
        self._node_handles = await self._subscription.subscribe_data_change(
            nodes=self._opcnodes,
            attr=ua.AttributeIds.Value,  # the attribute i am interested in
            queuesize=50,  # the queuesize should be bigger then the number of changes within a publishinterval, in this case 50 valuechanges per 1000 ms
            monitoring=ua.MonitoringMode.Reporting
        )
        self._subscribedNodes = set(Nodes)
        self._opcsubscribed = True

    async def unsubscribe(self):
        """
        Unsubscribe from all previous made subscriptions.

        Returns
        -------
        None.

        """
        await self._subscription.unsubscribe(self._node_handles)
        self._subscribedNodes = set()
        self._opcsubscribed = False

    async def subscribe_add(self, Nodes):
        """
        Adds subscriptions. This function will first unsubscribe all nodes and then
        reconnect with the new list of nodes.

        Parameters
        ----------
        Nodes : OPCNodeID
            List with OPCNodeIDs to subscribe. The NodeIDs are strings of the form:
            'ns=Y;s=zzz', where Y is the OPCNameSpace number and zzz the OPCIdentifier.

        Returns
        -------
        None.

        """
        for eachNode in Nodes:
            if eachNode not in self._subscribedNodes:
                self._subscribedNodes.add(eachNode)
        await self.subscribe(self._subscribedNodes)

    async def subscribe_sub(self, Nodes):
        """
        Delets subscriptions. This function will first unsubscribe all nodes and then
        reconnect with the new list of nodes.

        Parameters
        ----------
        Nodes : OPCNodeID
            List with OPCNodeIDs to subscribe. The NodeIDs are strings of the form:
            'ns=Y;s=zzz', where Y is the OPCNameSpace number and zzz the OPCIdentifier.

        Returns
        -------
        None.

        """
        for eachNode in Nodes:
            self._subscribedNodes.discard(eachNode)
        await self.subscribe(self._subscribedNodes)

    async def close_connection(self):
        """
        Closes the connection to the OPC server and the MQTT broker.

        Returns
        -------
        None.

        """
        await self._opcclient.disconnect()
        logging.info("Publishing: 'disconnected'")
        rc, mid = self._mqttclient.publish(client_id, "disconnected")
        logging.info("Publishing: 'disconnected' returned rc =", rc, "mid = ", mid)
        logging.info("Disonnecting from broker", self._mqttbroker)
        self._mqttclient.disconnect()
        await asyncio.sleep(1)
        logging.info("Stopping message loop")
        self._mqttclient.loop_stop(force=False)
        self._connected = False


async def opc2mqtt():
    try:
        OPC2MqttIntstance = OPC2Mqtt()
        await OPC2MqttIntstance.open_connection('opc.tcp://DLPC104:48010/', "localhost")
#        await OPC2MqttIntstance.subscribe(["ns=2;s=Demo.Dynamic.Scalar.Int32", "ns=2;s=Demo.Dynamic.Scalar.Float", "ns=2;s=Demo.Dynamic.Scalar.String", "ns=2;s=Demo.Dynamic.Arrays.Int32"])
        await OPC2MqttIntstance.subscribe(["ns=2;s=Demo.Dynamic.Scalar.Int32", "ns=2;s=Demo.Dynamic.Scalar.Float", "ns=2;s=Demo.Dynamic.Scalar.String"])
        await OPC2MqttIntstance.subscribe_add(["ns=2;s=Demo.Dynamic.Arrays.Int32"])
        await OPC2MqttIntstance.subscribe_sub(["ns=2;s=Demo.Dynamic.Scalar.Float"])
        await OPC2MqttIntstance.subscribe_add(["ns=2;s=Demo.Static.Scalar.String", "ns=2;s=Demo.Static.Scalar.Float", "ns=2;s=Demo.Static.Scalar.Double"])
        while OPC2MqttIntstance._mqttclient.connected_flag:
            await asyncio.sleep(1)
    except BaseException as e:
        logging.error("Exception catched!", e)
        OPC2MqttIntstance._mqttclient.connected_flag = False
    finally:
        await OPC2MqttIntstance.unsubscribe()
        await OPC2MqttIntstance.close_connection()
        logging.info("End")
        logging.shutdown()
        sys.exit()


async def main():
    await opc2mqtt()


if __name__ == "__main__":
    asyncio.run(main())
